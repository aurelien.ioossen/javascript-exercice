/*CLASSE LIVRE */
function Livre(ref, libelle, prixUnitaire) {

    this.ref = ref;
    this.prixUnitaire = prixUnitaire;
    this.libelle = libelle;

}

/* INSTANCIATION LIVRE*/
var html = new Livre("M1802", "HTML", 12.99);
var javascript = new Livre("J4852", "JAVASCRIPT", 19.99);
var java = new Livre("J7963", "JAVA", 29.99);
var tabLivre = [html, javascript, java];

//remplissage tableau
function remplirTableBody() {


    var option = '<option selected disabled>SELECTIONNEZ UN LIVRE</option>';
    for (var i = 0; i < tabLivre.length; i++) {
        option = option + '<option value="' + i + '">' + tabLivre[i].libelle + '</option>';
    }


    document.getElementById("bodytab").innerHTML =
        '<tr>' +
        '<td><select id=\'choix1\' onchange="updateLivreRow(1)">' + option + '</select></td>' +
        '<td><input type=\'text\' id=\'ref1\'></td>' +
        '<td><input type=\'text\' id=\'quantite1\' onchange="updatePrixGlobal(1)"></td>' +
        '<td><input type=\'text\' id=\'prixUnitaire1\'></td>' +
        '<td><input type=\'text\' id=\'prixGlobal1\' value="0"></td>' +
        '<tr>' +

        '<tr>' +
        '<td><select id=\'choix2\' onchange="updateLivreRow(2)">' + option + '</select></td>' +
        '<td><input type=\'text\' id=\'ref2\'></td>' +
        '<td><input type=\'text\' id=\'quantite2\' onchange="updatePrixGlobal(2)"></td>' +
        '<td><input type=\'text\' id=\'prixUnitaire2\'></td>' +
        '<td><input type=\'text\' id=\'prixGlobal2\' value="0"></td>' +
        '<tr>' +

        '<tr>' +
        '<td><select id=\'choix3\' onchange="updateLivreRow(3)">' + option + '</select></td>' +
        '<td><input type=\'text\' id=\'ref3\'></td>' +
        '<td><input type=\'text\' id=\'quantite3\' onchange="updatePrixGlobal(3)"></td>' +
        '<td><input type=\'text\' id=\'prixUnitaire3\'></td>' +
        '<td><input type=\'text\' id=\'prixGlobal3\' value="0"></td>' +
        '<tr>'

}

/*LISTENER */

var monPattern = new RegExp("[a-z]{*}");
var text = "123";

monPattern.test(text);


function updateLivreRow(numberRow) {
    var livreSelected;
    var selectChanged = document.getElementById('choix' + numberRow);
    var libelleLivreSelected = selectChanged.options[selectChanged.selectedIndex].value;
    console.log(libelleLivreSelected);

    if (libelleLivreSelected == "0") {

        livreSelected = html;
        console.log("LIVRE SELECTIONNE : HTML");


    } else if (libelleLivreSelected == "1") {

        console.log("LIVRE SELECTIONNE : JAVASCRIPT");
        livreSelected = javascript;
    } else {

        console.log("LIVRE SELECTIONNE : JAVA");
        livreSelected = java;


    }

    //UPDATE TD De LA ROW SELECTIONNE
    document.getElementById("ref" + numberRow).value = livreSelected.ref;
    document.getElementById("prixUnitaire" + numberRow).value = livreSelected.prixUnitaire;
    document.getElementById("prixGlobal" + numberRow).value = livreSelected.prixUnitaire;


}


function updatePrixGlobal(numberRow) {
    var prix = document.getElementById('prixGlobal' + numberRow);
    var prixUnitaire = document.getElementById('prixUnitaire' + numberRow);
    var quantite = document.getElementById('quantite' + numberRow);
    prix.value = parseInt(prixUnitaire.value * quantite.value);


    updatePrixTotal();


}


function updatePrixTotal() {

    var prixglobal = document.getElementById("prixTotal");
    prixglobal.value = parseInt(document.getElementById("prixGlobal1").value) + parseInt(document.getElementById("prixGlobal2").value) + parseInt(document.getElementById("prixGlobal2").value) + parseInt(document.getElementById("prixGlobal3").value);
}


